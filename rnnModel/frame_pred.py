
import tensorflow as tf
from keras import backend as K 
sess = tf.Session()  
K.set_session(sess)  
from tensorflow.keras.layers import Conv3D, ConvLSTM2D ,BatchNormalization
from tensorflow.keras.models import Sequential , load_model
from tensorflow.keras.callbacks import Model

import numpy as np
import cv2
import os

USE_TPU = True # use the TPU environment to train model on colab

def to_tpu(build_func):
    def inner():
        model  = build_func()
        if USE_TPU and os.environ['COLAB_TPU_ADDR']:
            strategy = tf.contrib.tpu.TPUDistributionStrategy(
                tf.contrib.cluster_resolver.TPUClusterResolver(tpu='grpc://' + os.environ['COLAB_TPU_ADDR']))
            model = tf.contrib.tpu.keras_to_tpu_model(model, strategy=strategy)
        return model
    return inner    



def get_model():
    rnn = Sequential()
    rnn.add(ConvLSTM2D(40,(3,3) , input_shape= (None,None,None,3), name="InputLayer" , 
                             padding="same" , return_sequences= True))
    rnn.add(BatchNormalization())

    rnn.add(ConvLSTM2D(40,(3,3), padding="same" , return_sequences= True))
    rnn.add(BatchNormalization())

    rnn.add(ConvLSTM2D(40,(3,3), padding="same" , return_sequences= True))
    rnn.add(BatchNormalization())

    rnn.add(ConvLSTM2D(40,(3,3), padding="same" , return_sequences= True))
    rnn.add(BatchNormalization())

    rnn.add(Conv3D(1,(3,3,3), activation='sigmoid' , padding ="same",  data_format='channels_last') , name="OutputLayer")

    rnn.compile(loss="binary_crossentropy", optimizer="adadelta")
    rnn.summary()
    return rnn


def collect_frames():
    frames= []
    count = 1
    cap = cv2.VideoCapture(0) # you can put here a path to video
    while(count<450):
        ret,frame = cap.read()
        if ret:
            frames.append(frame)
            cv2.imshow("frame",frame)
            print(f"frame {count} collected! ")
            count+=1
            if cv2.waitKey(60) & 0xff == 27 : break
    cv2.destroyAllWindows()
    cap.release()	
    return frames

data = collect_frames()    
model = get_model()
data, data_y = np.asarray(data) , np.asarray(data.reverse())
#data , data_y = np.expand_dims(data,0) , np.expand_dims(data_y,0)
model.fit(data,data_y,
            batch_size = 28,
            epochs= 100, shuffle= True,
            validation_split=0.05)
model.save("rnn_frame_predict.hdf5")            









