
resource "google_container_cluster" "gke-cluster" {
  provider           = "google-beta"
  name               = "gke-tpu-cluster-gitlab"
  network            = "default"
  zone               = "us-central1-b" # need the specific zone to allow tpu
  initial_node_count = 3
  enable_tpu         = "True"
  ip_allocation_policy {  # nedded for api aliasing
    
  }
}