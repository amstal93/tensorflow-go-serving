# tensorflow go serving

A golang microservice for predicting next frame using tensorflow serving.  
using [Terraform](https://www.terraform.io/) for infrastructure to train on GCP.  
[![pipeline status](https://gitlab.com/motkeg/tensorflow-go-serving/badges/master/pipeline.svg)](https://gitlab.com/motkeg/tensorflow-go-serving/commits/master)[![coverage report](https://gitlab.com/motkeg/tensorflow-go-serving/badges/master/coverage.svg)](https://gitlab.com/motkeg/tensorflow-go-serving/commits/master)