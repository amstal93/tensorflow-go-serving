!#bin/bash

# $1 = the first argument that passed to script.
# it refer to the model name , assume it exist on : 'models/<model-name>'
docker run -d --name serving_base tensorflow/serving
docker cp models/$1 serving_base:/models/$1
docker commit --change "ENV MODEL_NAME {$1}" serving_base new_model
echo "new container ID is: new_model"
docker kill serving_base
docker run new_model