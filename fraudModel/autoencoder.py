import time
import tensorflow as tf
import numpy as np
import pandas as pd



from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Input, Dense,Flatten , Dropout
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.keras import regularizers

from csv_utils import CSV


RANDOM_SEED = 1011
CLASSES = 2
class Autoencoder():
    def __init__(self, input_dim):
        self.input_dim = input_dim 
        self.model = self.build_model()

    def build_model(self ):
        model  = Sequential()
        
        model.add(Dense(32,input_shape=(self.input_dim,),activation= "relu",activity_regularizer=regularizers.l1(10e-5)))
        model.add(Dense(64,activation="relu"))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(1, activation='softmax'))
        
        model.compile(optimizer='adam', loss='binary_crossentropy' ,metrics=['accuracy'])
        model.summary()   
        return model 


    def prepare_data(self,pathToData, y=False):
        data = CSV(pathToData)
        if y:
            label = data.df[y]
            data.drop(y)
            data.normalaize()
            self.train  , self.test =  data.to_train_test_y(label)  
        else:
            data.normalaize()
            self.train  , self.test = data.to_train_test()
        
       

    def Test(self):
        self.model.evaluate(self.test[0],self.test[1])    

    def __call__(self , epochs , batch_size , test=False):
        if not test:
            tensorboard = TensorBoard(log_dir=f'fraudModel/logs/{time.time()}',
                                                        batch_size=batch_size,
                                                        write_graph=True,
                                                        histogram_freq=5,
                                                        write_images=True,
                                                        write_grads=True)
            checkpointer  = ModelCheckpoint(filepath='fraudModel/fraud.best.hdf5', verbose = 1)
            self.history = self.model.fit(self.train[0], self.train[1],
                            epochs=epochs,
                            batch_size=batch_size,
                            shuffle=True,
                            validation_split=0.2,
                            #verbose=0,
                            callbacks=[tensorboard,checkpointer] )
        else:
            self.model = load_model("fraudModel/fraud.best.hdf5")
            self.Test()  


    def export_saved_model(self,pathToExport):
        tf.keras.backend.set_learning_phase(0)
        model = load_model("fraudModel/fraud.best.hdf5")
        with tf.keras.backend.get_session() as sess:
            tf.saved_model.simple_save(sess,pathToExport,
                                        inputs={'input': model.input},
                                        outputs={t.name: t for t in model.outputs})


        """ print(history.history)     
        predictions = self.model.predict(X_test_scaled)   
        mse = np.mean(np.power(X_test_scaled - predictions, 2), axis=1)
        return pd.DataFrame({'reconstruction_error': mse})      """  

